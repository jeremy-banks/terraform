resource "aws_security_group" "allow_all" {
  name   = "allow_all"
  tags { Name = "allow_all" }
  vpc_id = "${aws_vpc.default.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_egress" {
  name   = "allow_egress"
  tags { Name = "allow_egress" }
  vpc_id = "${aws_vpc.default.id}"

  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "public_ssh" {
  name   = "public_ssh"
  tags { Name = "public_ssh" }
  vpc_id = "${aws_vpc.default.id}"

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "private_ssh" {
  name   = "private_ssh"
  tags { Name = "private_ssh" }
  vpc_id = "${aws_vpc.default.id}"

    ingress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    egress {
        from_port   = 22
        to_port     = 22
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }
}

resource "aws_security_group" "web" {
  name   = "web"
  tags { Name = "web" }
  vpc_id = "${aws_vpc.default.id}"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port   = 1433#mssql db engine
        to_port     = 1433
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    egress {
        from_port   = 1434#mssql browser service
        to_port     = 1434
        protocol    = "udp"
        cidr_blocks = [ "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    egress {
        from_port   = 3306#mysql
        to_port     = 3306
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    egress {
        from_port   = 5432#postgresql
        to_port     = 5432
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

}

resource "aws_security_group" "db" {
  name   = "db"
  tags { Name = "db" }
  vpc_id = "${aws_vpc.default.id}"

    ingress {
        from_port   = 1433#mssql db engine
        to_port     = 1433
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    egress {
        from_port   = 1433
        to_port     = 1433
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    ingress {
        from_port   = 1434#mssql browser service
        to_port     = 1434
        protocol    = "udp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    egress {
        from_port   = 1434
        to_port     = 1434
        protocol    = "udp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    ingress {
        from_port   = 3306#mysql
        to_port     = 3306
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    egress {
        from_port   = 3306
        to_port     = 3306
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    ingress {
        from_port   = 5432#postgresql
        to_port     = 5432
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    egress {
        from_port   = 5432
        to_port     = 5432
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    egress {
        from_port = 80#web
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}" ]
    }

    egress {
        from_port = 443#web
        to_port = 443
        protocol = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}" ]
    }
}

resource "aws_security_group" "nagios" {
  name   = "nagios"
  tags { Name = "nagios" }
  vpc_id = "${aws_vpc.default.id}"

    ingress {
        from_port   = 5666
        to_port     = 5666
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }
    egress {
        from_port   = 5666
        to_port     = 5666
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    ingress {
        from_port   = 5666
        to_port     = 5666
        protocol    = "udp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }
    egress {
        from_port   = 5666
        to_port     = 5666
        protocol    = "udp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    ingress {
        from_port   = 161
        to_port     = 161
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }
    egress {
        from_port   = 161
        to_port     = 161
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    ingress {
        from_port   = 161
        to_port     = 161
        protocol    = "udp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }
    egress {
        from_port   = 161
        to_port     = 161
        protocol    = "udp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    ingress {
        from_port   = 162
        to_port     = 162
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }
    egress {
        from_port   = 162
        to_port     = 162
        protocol    = "tcp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }

    ingress {
        from_port   = 162
        to_port     = 162
        protocol    = "udp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }
    egress {
        from_port   = 162
        to_port     = 162
        protocol    = "udp"
        cidr_blocks = [ "${var.aws_subnet_cidr[1]}", "${var.aws_subnet_cidr[2]}", "${var.aws_subnet_cidr[3]}" ]
    }
}
