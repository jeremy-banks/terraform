#based on https://github.com/terraform-providers/terraform-provider-aws/blob/master/examples/two-tier/main.tf
resource "aws_instance" "wordpress_db" {
  tags { Name = "wordpress_db" }
  depends_on        = ["aws_instance.bastion_host"]

  instance_type     = "t2.micro"
  ami               = "ami-0bbe6b35405ecebdb"

  key_name          = "${aws_key_pair.bootstrap.id}"

  vpc_security_group_ids = [
    "${aws_security_group.allow_egress.id}",
    "${aws_security_group.db.id}",
    "${aws_security_group.public_ssh.id}"
    ]

  subnet_id         = "${aws_subnet.tier3.id}"

  provisioner "remote-exec" {
    inline = [
      #https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce
      "sudo apt-get remove docker docker-engine docker.io",
      "sudo apt-get update -y",
      "sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable'",
      "sudo apt-get update -y",
      "sudo apt-get install -y docker-ce",
      "sudo mkdir -p /locdrv/dockervol/some-percona/data /locdrv/dockervol/some-percona/cnf /locdrv/dockervol/some-percona/tmp /locdrv/dockervol/some-percona/log",
      "sudo chmod 1777 /locdrv/dockervol/some-percona/tmp",
      #https://hub.docker.com/_/percona/
      "sudo docker run -d --name some-percona -v /locdrv/dockervol/some-percona/cnf:/etc/mysql/conf.d -v /locdrv/dockervol/some-percona/data:/var/lib/mysql -v /locdrv/dockervol/some-percona/log:/var/log -v /locdrv/dockervol/some-percona/tmp:/tmp -e MYSQL_ROOT_PASSWORD=mysecretpw -e MYSQL_DATABASE=mydatabase -e MYSQL_USER=mysqluser -e MYSQL_PASSWORD=mysqlpassword percona:5.5.61 --innodb_buffer_pool_size=128M"
    ]

    connection {
      agent = false
      bastion_host = "${aws_instance.bastion_host.public_ip}"
      bastion_user = "ubuntu"
      bastion_port = 22
      bastion_private_key = "${file("~/repos/terraform/keys/id_rsa")}"
      user = "ubuntu"
      private_key = "${file("~/repos/terraform/keys/id_rsa")}"
    }
  }
}
