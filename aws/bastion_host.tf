#based on https://github.com/terraform-providers/terraform-provider-aws/blob/master/examples/two-tier/main.tf
resource "aws_instance" "bastion_host" {
  tags { Name = "bastion_host" }

  instance_type     = "t2.micro"
  ami               = "ami-0bbe6b35405ecebdb"

  key_name          = "${aws_key_pair.bootstrap.id}"

  vpc_security_group_ids = [
    "${aws_security_group.allow_egress.id}",
    "${aws_security_group.public_ssh.id}",
    "${aws_security_group.web.id}"
    ]

  subnet_id         = "${aws_subnet.tier1.id}"

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get -y update",
      "sudo apt-get -y install nginx",
      "sudo service nginx start"
    ]

    connection {
      agent = false
      user = "ubuntu"
      private_key = "${file("~/repos/terraform/keys/id_rsa")}"
    }
  }
}
