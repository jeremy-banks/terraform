#create a VPC
resource "aws_vpc" "default" {
  cidr_block = "${var.aws_vpc_cidr}"
}

#create internet gateway for subnets to access internet
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

#grant main route table internet access
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.default.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.default.id}"
}

#eip needed for nat gateway
resource "aws_eip" "nat_gateway" {
  vpc      = true
  depends_on = ["aws_internet_gateway.default"]
}

#need nat gateway for private subnets to speak to internet
resource "aws_nat_gateway" "gw" {
  tags { Name = "gw NAT" }
  subnet_id     = "${aws_subnet.tier1.id}"
  allocation_id = "${aws_eip.nat_gateway.id}"
  depends_on = ["aws_internet_gateway.default"]
}

#create private route table
resource "aws_route_table" "private_route_table" {
    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "Private route table"
    }
}

#assign routes to private route table
resource "aws_route" "private_route" {
	route_table_id  = "${aws_route_table.private_route_table.id}"
	destination_cidr_block = "0.0.0.0/0"
	nat_gateway_id = "${aws_nat_gateway.gw.id}"
}

#associate tier1 subnet to public route table
resource "aws_route_table_association" "tier1_subnet" {
    subnet_id = "${aws_subnet.tier1.id}"
    route_table_id = "${aws_vpc.default.main_route_table_id}"
}

#associate tier2 subnet to private route table
resource "aws_route_table_association" "tier2_subnet" {
    subnet_id = "${aws_subnet.tier2.id}"
    route_table_id = "${aws_route_table.private_route_table.id}"
}

#associate tier3 subnet to private route table
resource "aws_route_table_association" "tier3_subnet" {
    subnet_id = "${aws_subnet.tier3.id}"
    route_table_id = "${aws_route_table.private_route_table.id}"
}

#tier1, internet-facing, load balancers, web pages, and a single bastion host
resource "aws_subnet" "tier1" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "${var.aws_subnet_cidr[1]}"
  tags { Name = "tier1" }
  map_public_ip_on_launch = true
}

#tier2, applications, logic
resource "aws_subnet" "tier2" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "${var.aws_subnet_cidr[2]}"
  tags { Name = "tier2" }
  map_public_ip_on_launch = false
}

#tier3, databases
resource "aws_subnet" "tier3" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "${var.aws_subnet_cidr[3]}"
  tags { Name = "tier3" }
  map_public_ip_on_launch = false
}
