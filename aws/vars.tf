variable "aws_default_region" {
  type = "string"
  default = "us-west-2"#US West (Oregon) Region https://rds.us-west-2.amazonaws.com
}

variable "aws_vpc_cidr" {
  type = "string"
  default = "10.0.0.0/16"
}

variable "aws_subnet_cidr" {
  type = "list"
  default = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24" ]
}
