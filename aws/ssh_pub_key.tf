resource "aws_key_pair" "bootstrap" {
  key_name   = "bootstrap-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1sEjkWb/CUe8NHO/dWdjjBDw+ziyX5MUMo/EdrWnpZZvL95/6PiM0MxcalPH+5kqDqdpZxqzWgPmvQVWtgFPcsj2WAcC2yhNd+Y35VOP19+NQP+si1J5+RGBskfwTPbksUp8Uwe8D0VdkwWGo1ZRQpnHbKD+QWew5BFinoxOqAVoNTcaY84upwAei0tuqpYs9HjB7An8yj6Efl0ffruSvqHIM5xUCvo680kGO1Pljqlbahz1eTp37lha0tGUjdGTP2Vpb1Yn07/G8fpKIovscWQXfABRh4oUls4+OCVo5680j/AQmMA7z52sceSuo4Z5dVKVH3GEzF3RaYVZcb1I3"
}
