variable "vsphere_user" {
  type = "string"
  default = "root"
}

variable "vsphere_server" {
  type = "string"
  default = "192.168.2.6"
}

variable "vmname" {
  type = "string"
  default = "terraform-test"
}
