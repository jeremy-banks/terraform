#ubuntu1604
resource "vsphere_virtual_machine" "vm" {
  name             = "${var.vmname}"
  resource_pool_id = "${data.vsphere_resource_pool.pool.id}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"

  num_cpus = 2
  memory   = 2048
  guest_id = "other3xLinux64Guest"

  network_interface {
    network_id = "${data.vsphere_network.network.id}"
    mac_address = "02:00:00:00:00:14"
  }

  disk {
      label = "disk0"
      size = 20
  }

  cdrom {
    datastore_id = "${data.vsphere_datastore.datastore.id}"
    path         = "isos/ubuntu-16.04.3-server-amd64.iso"
  }
}
